//el automatizador de tareas grunt no me funciono bien
//por ende y por motivos de no atrazarme academicamente segui 
//trabajando con el automatizador de tareas gulp 
module.exports = function (grunt){
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        }
    });
    //agregar tareas, cargar los paquetes o pluggins que vamos a usar
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('css',['sass']);
};
