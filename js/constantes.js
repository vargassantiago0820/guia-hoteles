function base (){
    this.init();
}

base.prototype.init = function(){
    $('#banner').load("banner.html");
    $('#footer').load("footer.html");
}; 

$(document).ready(function(){
    new base();
});