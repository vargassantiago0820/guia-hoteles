$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 1000
    });
    /*respectivo al modal que se esta ejecutando desde el boton con data-target #contacto
    para mostrar ventana emergente con opciones de 
    formulario o mensajeria de primera necesidad*/
    $('#contacto').on('show.bs.modal', function(e){
        console.log('el modal se esta ejecutando');
        //cambiando el color del boton btnModalContacto removiendo tambien la clase del boton btn btn-outline-success
        $('#btnModalContacto').removeClass('btn btn-outline-success');
        $('#btnModalContacto').addClass('btn-primary');
        $('#btnModalContacto').prop('disable',true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('el modal se ejecuto');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('el modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('el modal se ocultó');
        $('#btnModalContacto').prop('disable',false);
    });
});